from enum import Enum

class Pikogramm(Enum):
    """Enums for ObjectDetection."""
    Taco = 0
    Stift = 1
    Schraubenschlussel = 2
    Lineal = 3
    Hammer = 4
    Eimer = 5
    Treppe = 6
    Piktohalter = 7