from enum import Enum
class SensorPosition(Enum):
    """Enums for define Sensor Positions."""
    topFrontLeft = 1
    topFrontRight = 2
    bottomFrontLeft = 3
    bottomFrontRight = 4
    topFrontMiddle = 5