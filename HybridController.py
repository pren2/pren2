from Hybrid.TheHybrid import TheHybrid
from Prozess.Phase1 import Phase1
from Prozess.Phase2 import Phase2
from Prozess.Phase3 import Phase3
from Hybrid.Socket.Server import HybridServer
import RPi.GPIO as GPIO
from Hybrid.Logging import Logging
import Enums

if __name__ == '__main__':
    #Init
    theHybrid = TheHybrid()
    HybridServer(theHybrid)
    Logging(theHybrid)

    #Wait for Pushed Button
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    theHybrid.blink(3)

    while True:
        if GPIO.input(17) == GPIO.LOW:
            #Init Phase1
            phase1 = Phase1(theHybrid)
            targetPiktogramm = phase1.getTargetPiktogramm()
            del phase1

            # Phase2
            phase2 = Phase2(theHybrid, Enums.Pikogramm.Lineal)
            del phase2

            # Phase3
            phase3 = Phase3.Phase3(theHybrid, targetPiktogramm)
            del phase3

            # Blink
            theHybrid.blink(3)

            # Stop
            break