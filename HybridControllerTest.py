from Hybrid.TheHybrid import TheHybrid
from Prozess.Phase1 import Phase1
from Prozess.Phase2 import Phase2
from Prozess.Phase3 import Phase3
import Enums
from Prozess.Test.TestSensor import TestSensor
from Prozess.Test.TestObjectDetection import TestObjectDetection
from Hybrid.Socket.Server import HybridServer
from Hybrid.Logging import Logging

if __name__ == '__main__':

    #Init
    #theHybrid = TheHybrid()
    #TestTransmitter(theHybrid)
    #TestSensor(theHybrid)

    #theHybrid = FakeHybrid()
    #logger = Logging(theHybrid)
    #TestTransmitter(theHybrid)
    #HybridServer(theHybrid)
    # Client()
    #FakeProzess(theHybrid)

    # Test
    theHybrid = TheHybrid()
    #Logging(theHybrid)
    HybridServer(theHybrid)

    #theHybrid.sendCommand(Enums.Command.TurnLeft)
    #theHybrid.sendCommand(Enums.Command.TurnRight)
    #theHybrid.sendCommand(Enums.Command.TurnRight)
    #theHybrid.drive45Right()
    #theHybrid.drive45Left()

    #phase1 = Phase1(theHybrid)
    #targetPiktogramm = Phase1.getTargetPiktogramm()
    #TestSensor(theHybrid)
    #print(targetPiktogramm)
    #TestObjectDetection(theHybrid)

    #theHybrid.changeModelPathToDistance()

    #Phase2(theHybrid, Enums.Pikogramm.Lineal)
    #Phase3.Phase3(theHybrid, Enums.Pikogramm.Hammer)
