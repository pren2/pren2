from Prozess.Prozess import Process
import Enums


class Phase2(Process):
    """Phase to find fly of the top from the Stair."""
    def __init__(self, theHybrid, targetPiktogramm):
        super().__init__()
        self.target = targetPiktogramm
        self.theHybrid = theHybrid
        self.theHybrid.observeObjectDetection(self)
        self.MAX_STEPS = 5
        self.step_counter = 0
        self.MAX_HIGH = 100
        self.TOLERANZ_PERCENT = 20 / 100
        self.HIGH_UPPER = self.MAX_HIGH
        self.HIGH_DOWNER = self.MAX_HIGH - self.MAX_HIGH * self.TOLERANZ_PERCENT
        self.TRITTHOHE = 7  # cm
        self.flewForward = False
        self.run()

    def update_objectdetection(self, subjectobjectdetection):
        if subjectobjectdetection.get_piktogramm() == self.target:
            self.alignToObject(subjectobjectdetection)

    def run(self):
        ausgangslage_distance_bottom = self.theHybrid.getSensorMessure(Enums.SensorPosition.bottomFrontLeft)

        while True:
            #Messure Distance to ground
            subjectSensorBottomLeft = self.theHybrid.getSensorMessure(Enums.SensorPosition.bottomFrontLeft)

            if self.flewForward:
                #Check if new Step reached
                differenz = subjectSensorBottomLeft.get_distance()-ausgangslage_distance_bottom.get_distance()
                if differenz < self.TRITTHOHE * -1:
                    print("New Step reached")
                    self.step_counter = self.step_counter + 1


            #Check High
            if subjectSensorBottomLeft.get_distance() > self.HIGH_UPPER:
                self.theHybrid.sendCommand(Enums.Command.FlyDown)
                self.flewForward = False
            elif subjectSensorBottomLeft.get_distance() < self.HIGH_DOWNER:
                self.theHybrid.sendCommand(Enums.Command.FlyUp)
                self.flewForward = False
            else:
                #High Ok -> Fly Forward
                self.theHybrid.sendCommand(Enums.Command.FlyForward)
                self.flewForward = True

            #Check if all Steps reached
            if self.step_counter > self.MAX_STEPS:
                print("Alle Treppen erreicht")
                self.theHybrid.unregisterObjectDetection(self)
                break

            ausgangslage_distance_bottom = subjectSensorBottomLeft


    def alignToObject(self, subjectObjectDetection):
        """Methode to align to object"""
        (startX, startY, endX, endY) = subjectObjectDetection.get_objectBox()

        # caluclate middle bottom
        middleXObject = (startX + endX) / 2
        frameheight, framewidth, channel = subjectObjectDetection.get_orgShape()
        frameMiddlePointX = framewidth / 2
        leftMiddleXRange = frameMiddlePointX - (framewidth * 0.1)
        rightMiddleXRange = frameMiddlePointX + (framewidth * 0.1)
        if middleXObject < leftMiddleXRange:
            self.theHybrid.sendCommand(Enums.Command.FlyLeft)
        elif middleXObject >= leftMiddleXRange and middleXObject <= rightMiddleXRange:
            #Straight Ahead
            pass
        else:
            self.theHybrid.sendCommand(Enums.Command.FlyRight)