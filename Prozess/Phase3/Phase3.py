import Enums
from Prozess.Phase3.DriveToTarget import DriveToTarget
from Prozess.Phase3.Land import Land
from Prozess.Phase3.GetDistanceToTarget import GetDistanceToTarget
from Prozess.Phase3.AlignToTarget import AlignToTarget
from Prozess.Phase3.ReachHigh import ReachHigh

class Phase3:
    """Process to get in front of the target Piktogramm."""
    def __init__(self, theHybrid, targetPiktogramm):
        self.theHybrid = theHybrid
        self.targetPiktogramm = targetPiktogramm
        self.orderOfPiktogramms = [
            Enums.Pikogramm.Hammer,
            Enums.Pikogramm.Taco,
            Enums.Pikogramm.Lineal,
            Enums.Pikogramm.Eimer,
            Enums.Pikogramm.Stift
        ]
        self.run()

    def run(self):
        #Fly To Target
        indexTarget = self.orderOfPiktogramms.index(self.targetPiktogramm)
        if indexTarget == 0:
            indexTarget = 1
        elif indexTarget == len(self.orderOfPiktogramms) - 1:
            indexTarget = len(self.orderOfPiktogramms)-2
        alignToTarget = AlignToTarget(self.theHybrid, self.orderOfPiktogramms[indexTarget], self.orderOfPiktogramms)
        del alignToTarget


        # Reach Correct High
        reachHigh = ReachHigh(self.theHybrid)
        del reachHigh

        #Get right Distance to Target
        getDistanceToTarget = GetDistanceToTarget(self.theHybrid)
        del getDistanceToTarget

        #Land
        land = Land(self.theHybrid)
        del land

        #DriveToTarget
        driveToTarget = DriveToTarget(self.theHybrid, self.orderOfPiktogramms, self.targetPiktogramm)
        del driveToTarget