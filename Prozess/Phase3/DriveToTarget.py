from Prozess.Prozess import Process
import Enums
import time
from time import sleep


class DriveToTarget(Process):
    """Process to drive to the target Piktogramm."""
    def __init__(self, theHybrid, orderOfPiktogramms, targetPikto):
        super().__init__()
        print("Drive To Target")
        self.theHybrid = theHybrid
        self.orderOfPiktogramms = orderOfPiktogramms
        self.target = targetPikto
        self.run()

    def run(self):
        while True:
            indexTarget = self.orderOfPiktogramms.index(self.target)

            if indexTarget == 0:
                #Target is on the left outter side
                for x in range(2):
                    self.theHybrid.sendCommand(Enums.Command.TurnLeft)
                self.theHybrid.sendCommand(Enums.Command.DriveForward)
                time.sleep(1.5)
            elif indexTarget == len(self.orderOfPiktogramms) - 1:
                # Target is on the right outter side
                for x in range(2):
                    self.theHybrid.sendCommand(Enums.Command.TurnRight)
                self.theHybrid.sendCommand(Enums.Command.DriveForward)
                time.sleep(1.5)
            else:
                #Target is in front
                self.theHybrid.sendCommand(Enums.Command.DriveForward)
                time.sleep(1)

            self.theHybrid.sendCommand(Enums.Command.Stop)
            break