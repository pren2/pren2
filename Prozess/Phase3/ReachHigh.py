from Prozess.Prozess import Process
import Enums


class ReachHigh(Process):
    """Process to reach Max High."""
    def __init__(self, theHybrid):
        super().__init__()
        print("Reach High")
        self.theHybrid = theHybrid
        self.DISTANCE_DOWNER = 43  # cm
        self.DISTANCE_UPPER = 53  # cm
        self.run()

    def run(self):
        while True:
            # Messure
            distance = self.theHybrid.getSensorMessure(Enums.SensorPosition.bottomFrontLeft).get_distance()

            # Decide
            if distance > self.DISTANCE_DOWNER and distance < self.DISTANCE_UPPER:
                break
            if distance < self.DISTANCE_DOWNER:
                self.theHybrid.sendCommand(Enums.Command.FlyUp)
            elif distance > self.DISTANCE_UPPER:
                self.theHybrid.sendCommand(Enums.Command.FlyDown)