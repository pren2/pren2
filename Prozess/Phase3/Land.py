from Prozess.Prozess import Process
import Enums

class Land(Process):
    """Process to drive to land Hybrid."""
    def __init__(self, theHybrid):
        super().__init__()
        print("Land")
        self.theHybrid = theHybrid
        self.DISTANCE_UPPER = 5  # cm
        self.run()

    def run(self):
        # Todo: Befehl um die Propeller auszuschalten?
        while True:
            #Messure
            distance = self.theHybrid.getSensorMessure(Enums.SensorPosition.bottomFrontLeft).get_distance()

            if distance > self.DISTANCE_UPPER:
                self.theHybrid.sendCommand(Enums.Command.FlyDown)
            else:
                break