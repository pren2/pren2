from Prozess.Prozess import Process
import Enums
import queue
from datetime import datetime


class AlignToTarget(Process):
    """Process to align to the target Piktogramm."""
    def __init__(self, theHybrid, targetPiktogramm, orderOfPiktogramms):
        super().__init__()
        print("AlignToTarget")
        self.theHybrid = theHybrid
        self.targetPiktogramm = targetPiktogramm
        self.theHybrid.observeObjectDetection(self)
        self.orderOfPiktogramms = orderOfPiktogramms
        self.objectDetectionQueue = queue.Queue()
        self.run()

    def update_objectdetection(self, subjectobjectdetection):
        self.objectDetectionQueue.put(subjectobjectdetection)

    def run(self):
        ausgangslage = datetime.now()
        indexTarget = self.orderOfPiktogramms.index(self.targetPiktogramm)

        while True:
            try:
                subject = self.objectDetectionQueue.get(0)
                if subject is not None and subject.get_timestamp() > ausgangslage:

                    subjectPiktogrammIndex = self.orderOfPiktogramms.index(subject.get_piktogramm())

                    if subjectPiktogrammIndex == indexTarget:
                        #Target was seen
                        #Get position from picture
                        objectPosition = self.getObjectPosition(subject)

                        if objectPosition == "Middle":
                            self.theHybrid.unregisterObjectDetection(self)
                            break
                        elif objectPosition == "Left":
                            self.theHybrid.sendCommand(Enums.Command.FlyLeft)
                        else:
                            self.theHybrid.sendCommand(Enums.Command.FlyRight)

                    elif subjectPiktogrammIndex > indexTarget:
                        self.theHybrid.sendCommand(Enums.Command.FlyLeft)
                    else:
                        print("Not Target")
                        self.theHybrid.sendCommand(Enums.Command.FlyRight)

                    with self.objectDetectionQueue.mutex:
                        self.objectDetectionQueue.queue.clear()

            except queue.Empty:
                pass

    def getObjectPosition(self, subjectObjectDetection):
        (startX, startY, endX, endY) = subjectObjectDetection.get_objectBox()

        # caluclate middle bottom
        middleXObject = (startX + endX) / 2
        frameheight, framewidth, channel = subjectObjectDetection.get_orgShape()
        frameMiddlePointX = framewidth / 2
        leftMiddleXRange = frameMiddlePointX - (framewidth * 0.08)
        rightMiddleXRange = frameMiddlePointX + (framewidth * 0.08)
        if middleXObject < leftMiddleXRange:
            return "Left"
        elif middleXObject >= leftMiddleXRange and middleXObject <= rightMiddleXRange:
            return "Middle"
            pass
        else:
            return "Right"
