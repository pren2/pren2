from Prozess.Prozess import Process
import Enums


class GetDistanceToTarget(Process):
    """Process to fly in front to the target Piktogramm."""
    def __init__(self, theHybrid):
        super().__init__()
        print("Get Distance To Target")
        self.theHybrid = theHybrid
        self.DISTANCE_UPPER = 48  # cm
        self.DISTANCE_DOWNER = 38  # cm
        self.run()

    def run(self):
        while True:
            #Messure
            distance = self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontLeft).get_distance()

            #Decide
            if distance > self.DISTANCE_DOWNER and distance < self.DISTANCE_UPPER:
                break
            if distance < self.DISTANCE_DOWNER:
                self.theHybrid.sendCommand(Enums.Command.FlyBackward)
            elif distance > self.DISTANCE_UPPER:
                self.theHybrid.sendCommand(Enums.Command.FlyForward)
