import Enums
from Prozess.Prozess import Process

class FindPiktogramm(Process):
    """Process to find the Piktogramm."""
    def __init__(self, theHybrid):
        super().__init__()
        self.theHybrid = theHybrid
        self.theHybrid.observeObjectDetection(self)
        self.piktogramm = None
        self.run()

    def update_objectdetection(self, subjectobjectdetection):
        self.piktogramm = subjectobjectdetection.get_piktogramm()
        self.theHybrid.blink(self.piktogramm.value+2)

    def run(self):
        turnRights = 0

        # Search piktogramm
        while self.piktogramm is None:
            if turnRights >= 24:
                break
            print("FindPiktogramm: Suche...")
            self.theHybrid.sendCommand(Enums.Command.TurnRight)
            turnRights = turnRights + 1

        if self.piktogramm is not None:
            print(self.piktogramm.name + "erkannt")

        # Go to startposition
        if turnRights <= 12:
            for x in range(turnRights):
                self.theHybrid.sendCommand(Enums.Command.TurnLeft)
        else:
            restlicheRights = 24 - turnRights
            for x in range(restlicheRights):
                self.theHybrid.sendCommand(Enums.Command.TurnRight)

        #Unregister from ObjectDetetion
        self.theHybrid.unregisterObjectDetection(self)
        print("Zur Ausgangslage zurückgedreht")

    def getTargetPiktogramm(self):
        return self.piktogramm