from Prozess.Prozess import Process
import Enums
import time


class FindMiddleStair(Process):
    """Process to drive to Middel of Stair."""
    def __init__(self, theHybrid):
        super().__init__()
        print("Start FindMiddleStair")
        self.theHybrid = theHybrid
        self.toleranzWert = 1000 #75
        self.turns = 2
        self.run()

    def run(self):
        #Messure left Site
        self.theHybrid.drive45Left()
        messureLeft = self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontLeft).get_distance()
        print("Messung Links")
        print("Links gemessen "+str(messureLeft))
        print("Rechts gemessen "+str(self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontRight).get_distance()))
        self.theHybrid.drive45Right()

        if messureLeft > self.toleranzWert:
            #No Left side detected -> drive to Middle
            self.theHybrid.drive90Right()
            self.driveToMiddle()
            self.theHybrid.drive90Left()
        else:
            # Messure right Site
            self.theHybrid.drive45Right()
            messureRight = self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontRight).get_distance()
            print("Messung REchts")
            print("Rechts gemessen " + str(messureRight))
            print("Links gemessen " + str(self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontLeft).get_distance()))
            self.theHybrid.drive45Left()

            if messureRight > self.toleranzWert:
                # No right side detected -> drive to Middle
                self.theHybrid.drive90Left()
                self.driveToMiddle()
                self.theHybrid.drive90Right()

        print("Mitte gefunden")


    def driveToMiddle(self):
        self.theHybrid.sendCommand(Enums.Command.DriveForward)
        time.sleep(2)
        self.theHybrid.sendCommand(Enums.Command.Stop)