from Prozess.Prozess import Process
import Enums

class GoToStair(Process):
    """Process to drive to the Stair."""
    def __init__(self, theHybrid, dinstanceUpper, dinstaceDowner):
        super().__init__()
        self.theHybrid = theHybrid
        self.DISTANCE_UPPER = dinstanceUpper
        self.DISTANCE_DOWNER = dinstaceDowner
        self.run()

    def run(self):
        while True:
            #Messure distance
            distance = self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontLeft).get_distance()

            #decide
            if distance > self.DISTANCE_DOWNER and distance < self.DISTANCE_UPPER:
                self.theHybrid.sendCommand(Enums.Command.Stop)
                break
            elif distance < self.DISTANCE_DOWNER:
                self.theHybrid.sendCommand(Enums.Command.DriveBack)
            elif distance > self.DISTANCE_UPPER:
                self.theHybrid.sendCommand(Enums.Command.DriveForward)

        print("Zur Treppe gefahren")

