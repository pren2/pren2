from Prozess.Phase1.FindPikrogramm import FindPiktogramm
from Prozess.Phase1.GoToStair import GoToStair
from Prozess.Phase1.AlignToStair import AlignToStair
from Prozess.Phase1.FindMiddleStair import FindMiddleStair
import Enums

class Phase1:
    """Phase to find Piktogramm and drive in front of the stair."""
    def __init__(self, theHybrid):
        self.theHybrid = theHybrid
        self.targetPiktogramm = None
        self.run()

    def run(self):

        #Init FindPiktogramm
        findPiktogramm = FindPiktogramm(self.theHybrid)
        self.targetPiktogramm = findPiktogramm.getTargetPiktogramm()

        #Init GoToStair
        goToStair = GoToStair(self.theHybrid, 56, 51)
        del goToStair

        #Init alignToStair
        alignToStair1 = AlignToStair(self.theHybrid)
        del alignToStair1

        #Init FindMiddleStair
        findMiddle = FindMiddleStair(self.theHybrid)
        del findMiddle

        #If not Piktogramm was first seen
        if self.targetPiktogramm == None:
            print("Nichts gefunden - erneuter Versuch")
            findPiktogramm.run()
            self.targetPiktogramm = findPiktogramm.getTargetPiktogramm()
        del findPiktogramm

        #Fallback if still no Piktogramm was seen
        if self.targetPiktogramm == None:
            self.targetPiktogramm = Enums.Pikogramm.Lineal

        #Init alignToStair
        alignToStair2 = AlignToStair(self.theHybrid)
        del alignToStair2

    def getTargetPiktogramm(self):
        return self.targetPiktogramm
