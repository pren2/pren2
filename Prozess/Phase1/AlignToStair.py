from Prozess.Prozess import Process
import Enums


class AlignToStair(Process):
    """Process to alignt to Stair."""
    def __init__(self, theHybrid):
        super().__init__()
        print("Start alignToStair")
        self.theHybrid = theHybrid
        self.TOLERANZ_PERCENT = 1.25/100
        self.run()

    def run(self):

        while True:
            #Get Messures
            rightDistance = self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontRight).get_distance()
            leftDistance = self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontLeft).get_distance()

            #Calc Upper and Downer
            distanceUpper = rightDistance + rightDistance * self.TOLERANZ_PERCENT
            distanceDowner = rightDistance - rightDistance * self.TOLERANZ_PERCENT

            print("Linker Sensor " + str(leftDistance))
            print("Rechter Sensor " + str(rightDistance))
            print("Distance Upper " + str(distanceUpper))
            print("Distance Downer" + str(distanceDowner))
            print("--------------------------")

            #Decide
            if leftDistance > distanceUpper:
                self.theHybrid.sendCommand(Enums.Command.TurnRight)
            elif leftDistance < distanceDowner:
                self.theHybrid.sendCommand(Enums.Command.TurnLeft)
            else:
                print("Zur Treppe ausgerichtet")
                break