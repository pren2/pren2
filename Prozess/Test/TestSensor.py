from Prozess.Prozess import Process
import Enums


class TestSensor(Process):
    def __init__(self, theHybrid):
        self.theHybrid = theHybrid
        self.run()

    def run(self):
        while(True):
            print(self.theHybrid.getSensorMessure(Enums.SensorPosition.topFrontLeft).print())
