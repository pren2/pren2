from Prozess.Prozess import Process
from Transmitter import Transmitter
import Enums
import time
from time import sleep


class TestTransmitter(Process):
    def __init__(self, theHybrid):
        self.theHybrid = theHybrid
        self.run()

    def run(self):
        Transmitter.sendCommand(self, Enums.Command.Stop)
        Transmitter.sendCommand(self, Enums.Command.DriveForward)
        time.sleep(2)
        Transmitter.sendCommand(self, Enums.Command.DriveBack)
        time.sleep(2)
        Transmitter.sendCommand(self, Enums.Command.TurnRight)
        time.sleep(2)
        Transmitter.sendCommand(self, Enums.Command.TurnLeft)
        time.sleep(2)
        Transmitter.sendCommand(self, Enums.Command.Stop)
