from edgetpu.detection.engine import DetectionEngine
from imutils.video import VideoStream
from PIL import Image
import imutils
import cv2
import threading
from datetime import datetime
import Enums

class SubjectObjectDetection:
    """Subject witch will send to Observers."""
    def __init__(self, piktogramm, objectBox, timestamp, score, orgShape, distance):
        self.piktogramm = piktogramm
        self.objectBox = objectBox
        self.timestamp = timestamp
        self.score = score
        self.orgShape = orgShape
        self.distance = distance

    def get_piktogramm(self):
        return self.piktogramm

    def get_objectBox(self):
        return self.objectBox

    def get_timestamp(self):
        return self.timestamp

    def get_score(self):
        return self.score * 100

    def get_orgShape(self):
        return self.orgShape

    def get_distance(self):
        return self.distance

    def print(self):
        print("SubjectObjectDetection[ ", self.piktogramm, ", Score ", self.score, ", Distance ", self.distance, "]")

class ObserverObjectDetection:
    """Class for Observerpattern."""
    def update_objectdetection(self, subjectobjectdetection):
        pass

class ObjectDetection:
    """Class for Object Detection. Register to be notified."""
    def __init__(self, confidence=0.5, debugMode=False):
        self.model_path = 'Hybrid/ObjectDetection/models/nah.tflite'
        self.label_path = 'Hybrid/ObjectDetection/models/labels_all.txt'
        self.confidence = confidence
        self.debugMode = debugMode
        self.observers = []
        self.SENSOR_HEIGHT_MM = 2.76
        self.FOCAL_LENGTH_MM = 3.04
        threading.Thread(target=self.analyse).start()

    def register_observer(self, observerobjectdetection):
        self.observers.append(observerobjectdetection)

    def unregister_observer(self, observerObjectDetetction):
        self.observers.remove(observerObjectDetetction)

    def notify_observers(self, subject_object_detection):
        for observer in self.observers:
            observer.update_objectdetection(subject_object_detection)

    def analyse(self):
        """Methode witch loop over stream and recognice Objects."""
        # load the Google Coral object detection model
        model = DetectionEngine(self.model_path)

        vs = VideoStream(src=0).start()
        while True:

            # grab the frame from the threaded video stream and resize it
            frame = vs.read()
            frame = imutils.resize(frame, width=500)
            orig = frame.copy()

            # prepare the frame for object detection by converting (1) it
            # from BGR to RGB channel ordering and then (2) from a NumPy
            # array to PIL image format
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)

            # make predictions on the input frame / messure Coral
            results = model.detect_with_image(frame, self.confidence, keep_aspect_ratio=True, relative_coord=False)

            for r in results:
                # extract the bounding box and box and predicted class label
                box = r.bounding_box.flatten().astype("int")
                label = Enums.Pikogramm(r.label_id)

                #Calc distance
                distance = self.calcDistance(orig.shape, box, label)

                #Notify observers
                subject = SubjectObjectDetection(label, box, datetime.now(), r.score, orig.shape, distance)
                self.notify_observers(subject)

    def calcDistance(self, image_shape, objectBox, piktogramm):
        """Calc Distance to recognized Objects"""
        originHeigh = {
            Enums.Pikogramm.Hammer: 14,
            Enums.Pikogramm.Lineal: 16,
            Enums.Pikogramm.Eimer: 13,
            Enums.Pikogramm.Taco: 13,
            Enums.Pikogramm.Stift: 13,
            Enums.Pikogramm.Schraubenschlussel: -1
        }
        imageHigh, imageWidh = image_shape[:2]
        (startX, startY, endX, endY) = objectBox
        object_height = endY - startY

        distance = (self.FOCAL_LENGTH_MM * originHeigh[piktogramm] * imageHigh) / (object_height * self.SENSOR_HEIGHT_MM)
        return distance