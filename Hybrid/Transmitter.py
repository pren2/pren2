import time
import serial
from time import sleep

class Transmitter:
    """Class to transmitt Commands over UART"""
    def __init__(self):
        self.emergencyIsOff = True

    def sendCommand(self, command, senderToken = None):
        if self.emergencyIsOff:
            self.__transmitCommand(command)
        if self.emergencyIsOff is False and senderToken is not None:
            self.__transmitCommand(command)

    def __transmitCommand(self, command):
        ser = serial.Serial("/dev/ttyS0", 115200)

        # Convert String to Bytes
        res = bytes(str(command.value), 'utf-8')
        print("send Command", command, " Code: ", res)

        #transmitt Command
        ser.write(res)

        #Wait for execute Command
        time.sleep(0.75)

    def setEmergencyOn(self):
        self.emergencyIsOff = False
        print("Emergency is On")