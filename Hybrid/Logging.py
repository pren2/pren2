import queue
from datetime import datetime
import threading
import Enums
import time
from time import sleep

class Logging:
    """Class to Log High over the Operation."""
    def __init__(self, theHybrid):
        now = datetime.now()
        dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")
        self.theHybrid = theHybrid
        self.filename = "log/log_"+dt_string+".txt"
        threading.Thread(target=self.run).start()

    def run(self):
        while True:
            self.logSensorSobject(self.theHybrid.getSensorMessure(Enums.SensorPosition.bottomFrontLeft))
            self.logSensorSobject(self.theHybrid.getSensorMessure(Enums.SensorPosition.bottomFrontRight))
            time.sleep(1.5)

    def logSensorSobject(self, subject_sensor=None):
        string = "SensorPosition" + str(subject_sensor.get_sensorPosition()) + " Time " + str(subject_sensor.get_timestamp()) + " Distance " + str(subject_sensor.get_distance()) + "\n"
        with open(self.filename, 'a') as file:
            file.write(string)
        file.close()

