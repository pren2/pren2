from datetime import datetime
import RPi.GPIO as GPIO
import time

class SubjectSensor:
    """Subject witch will send to Observers."""
    def __init__(self, sensorposition, timestamp, distance):
        self.timestamp = timestamp
        self.distance = distance
        self.sensorposition = sensorposition

    def get_sensorPosition(self):
        return self.sensorposition

    def get_distance(self):
        return self.distance

    def get_timestamp(self):
        return self.timestamp

    def print(self):
        print("SubjectSensor[ SensorPosition", self.sensorposition, "Time ", self.timestamp, " Distance ", self.distance, "]")

class SensorController:
    """Class witch contains Sensors."""
    def __init__(self):
        self.sensors = {}
        
    def addSensor(self, objectPosition, gipoTrigger, gipoEcho):
        self.sensors[objectPosition] = Sensor(objectPosition, gipoTrigger, gipoEcho)

    def getMessure(self, sensorposition):
        if self.sensors[sensorposition] != None:
            return self.sensors[sensorposition].getDistance()
        else:
            return None
    

class Sensor:
    """Class witch get Distance from Sensors."""
    def __init__(self, sensorPosition, gipoTrigger, gipoEcho):
        self.sensorPosition = sensorPosition
        self.GPIO_TRIGGER = gipoTrigger
        self.GPIO_ECHO = gipoEcho

    def get_sensorPosition(self):
        return self.sensorPosition

    def getDistance(self):
        # GPIO Modus (BOARD / BCM)
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        GPIO.setup(self.GPIO_TRIGGER, GPIO.OUT)
        GPIO.setup(self.GPIO_ECHO, GPIO.IN)

        # set Trigger to HIGH
        GPIO.output(self.GPIO_TRIGGER, True)

        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(self.GPIO_TRIGGER, False)

        StartZeit = time.time()
        StopZeit = time.time()

        while GPIO.input(self.GPIO_ECHO) == 0:
            StartZeit = time.time()

        while GPIO.input(self.GPIO_ECHO) == 1:
            StopZeit = time.time()

        # calc Distance
        TimeElapsed = StopZeit - StartZeit
        distanz = (TimeElapsed * 34300) / 2
        return SubjectSensor(self.sensorPosition, datetime.now(), distanz)