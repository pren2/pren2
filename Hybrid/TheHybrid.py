import threading
import Enums
from Hybrid.ObjectDetection.ObjectDetection import ObjectDetection
from Hybrid.Light import Light
from Hybrid.Transmitter import Transmitter
from Hybrid.Sensor import SensorController


class TheHybrid:
    """Class witch contains every Component and controlls it"""
    def __init__(self):
        #Init ObjectDetection
        self.objectDetection = ObjectDetection()

        #Init Sensors
        self.sensorController = SensorController()
        self.sensorController.addSensor(Enums.SensorPosition.topFrontLeft, 27, 22)
        self.sensorController.addSensor(Enums.SensorPosition.topFrontRight, 24, 25)
        self.sensorController.addSensor(Enums.SensorPosition.bottomFrontLeft, 5, 6)
        self.sensorController.addSensor(Enums.SensorPosition.bottomFrontRight, 16, 20)

        self.light = Light(19)
        self.transmitter = Transmitter()

    def getSensorMessure(self, sensorPosition):
        """Get Messure from specific Sensore"""
        return self.sensorController.getMessure(sensorPosition)

    def observeObjectDetection(self, observerObjectDetection):
        self.objectDetection.register_observer(observerObjectDetection)

    def unregisterObjectDetection(self, observerObjectDetection):
        self.objectDetection.unregister_observer(observerObjectDetection)

    def sendCommand(self, command, senderToken = None):
        """Execute Command"""
        self.transmitter.sendCommand(command, senderToken)

    def setEmergencyOn(self):
        self.transmitter.setEmergencyOn()

    def drive45Left(self):
        for x in range(3):
            self.sendCommand(Enums.Command.TurnLeft)

    def drive45Right(self):
        for x in range(3):
            self.sendCommand(Enums.Command.TurnRight)

    def drive90Left(self):
        for x in range(2):
            self.drive45Left()

    def drive90Right(self):
        for x in range(2):
            self.drive45Right()

    def blink(self, blinknumber):
        threading.Thread(target=self.light.blink(blinknumber)).start()
