from datetime import datetime
import Enums
from Hybrid.Fake.FakeTransmitter import FakeTransmitter
import time
from Enums import Command


class FakeProzess:
    """Fakeimplemtation for Process class."""
    def __init__(self, theHybrid):
        self.theHybrid = theHybrid
        self.transmitter = FakeTransmitter()
        self.run()

    def update_sensor(self, subject_sensor):
        #subject_sensor.print()
        pass

    def run(self):
        print("Fake Prozess gestartet")
        while True:
            self.theHybrid.sendCommand(Command.DriveBack)
            time.sleep(1)
