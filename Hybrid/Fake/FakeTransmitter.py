import time
from time import sleep
from Enums import Command


class FakeTransmitter:
    """Fakeimplemtation for Transmitter class."""
    def __init__(self):
        self.emergencyIsOff = True

    def sendCommand(self, command, senderToken = None):
        if self.emergencyIsOff:
            print("send Command", command, " Code: ", command.value)
        if self.emergencyIsOff is False and senderToken is not None:
            print("send Command", command, " Code: ", command.value)

    def changeEmergency(self):
        if self.emergencyIsOff == True:
            self.emergencyIsOff = False
        else:
            self.emergencyIsOff = True
