import random
import threading
from datetime import datetime
import time


class SubjectSensor:
    """Fakeimplemtation for SubjectSensor class."""
    def __init__(self, sensorposition, timestamp, distance):
        self.timestamp = timestamp
        self.distance = distance
        self.sensorposition = sensorposition

    def get_objectposition(self):
        return self.sensorposition

    def get_distance(self):
        return self.distance

    def get_timestamp(self):
        return self.timestamp

    def print(self):
        print("SubjectSensor[ SensorPosition", self.sensorposition, "Time ", self.timestamp, " Distance ", self.distance, "]")

class Sensor:
    """Fakeimplemtation for Sensor class."""
    def __init__(self, ObjectPosition):
        self.objectPosition = ObjectPosition
        self.observers = []
        threading.Thread(target=self.run).start()

    def register_observer(self, observer_sensor):
        self.observers.append(observer_sensor)

    def notify_observers(self, subject_sensor):
        for observer in self.observers:
            observer.update_sensor(subject_sensor)

    def get_ObjectPosition(self):
        return self.objectPosition

    def run(self):
        while(True):
            time.sleep(3)
            self.notify_observers(SubjectSensor(self.objectPosition, datetime.now(), self._calcDistance()))

    def _calcDistance(self):
        return random.randint(3, 9)