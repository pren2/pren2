from edgetpu.detection.engine import DetectionEngine
from imutils.video import VideoStream
from PIL import Image
import argparse
import imutils
import time
import cv2
import threading
from datetime import datetime
import Enums


class SubjectObjectDetection:
    """Fakeimplemtation for SubjectObjectDetection class."""
    def __init__(self, piktogramm, objectBox, timestamp, score, orgShape, distance):
        self.piktogramm = piktogramm
        self.objectBox = objectBox
        self.timestamp = timestamp
        self.score = score
        self.orgShape = orgShape
        self.distance = distance
        self.FOCAL_LENGTH_MM = 3.04
        self.SENSOR_HEIGHT_MM = 2.76

    def get_piktogramm(self):
        return self.piktogramm

    def get_objectBox(self):
        return self.objectBox

    def get_timestamp(self):
        return self.timestamp

    def get_score(self):
        return self.score * 100

    def get_distance(self):
        return self.distance

    def get_orgShape(self):
        return self.orgShape

    def print(self):
        print("SubjectObjectDetection[ ", self.piktogramm, " Score ", self.score, "Distance ", self.distance,"]")


class ObserverObjectDetection:
    """Fakeimplemtation for ObserverObjectDetection class."""
    def update_objectdetection(self, subjectobjectdetection):
        pass


class ObjectDetection:
    """Fakeimplemtation for ObjectDetection class."""
    def __init__(self, confidence=0.3, debugMode=False):
        self.model_path = 'Hybrid/ObjectDetection/models/detectv3.tflite'
        self.label_path = 'Hybrid/ObjectDetection/models/labels_all.txt'
        self.confidence = confidence
        self.debugMode = debugMode
        self.observers = []
        self.labels = {}

        # Load LabelFile and Put in Diciationary
        # for row in open(self.label_path):
        #    (classID, label) = row.strip().split(maxsplit=1)
        #    self.labels[int(classID)] = label.strip()

        threading.Thread(target=self.analyse).start()

    def register_observer(self, observerobjectdetection):
        self.observers.append(observerobjectdetection)

    def notify_observers(self, subject_object_detection):
        for observer in self.observers:
            observer.update_objectdetection(subject_object_detection)

    def analyse(self):
        # loop over the frames from the video stream
        # load the Google Coral object detection model
        model = DetectionEngine(self.model_path)

        # initialize the video stream and allow the camera sensor to warmup
        vs = VideoStream(src=0).start()
        # vs = VideoStream(usePiCamera=False).start()
        while True:
            # Messure Zeit
            start_time = time.time()

            # grab the frame from the threaded video stream and resize it
            # to have a maximum width of 500 pixels
            frame = vs.read()
            frame = imutils.resize(frame, width=500)

            orig = frame.copy()

            # prepare the frame for object detection by converting (1) it
            # from BGR to RGB channel ordering and then (2) from a NumPy
            # array to PIL image format
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)

            # make predictions on the input frame / messure Coral
            results = model.detect_with_image(frame, self.confidence, keep_aspect_ratio=True, relative_coord=False)
            # results = model.DetectWithImage(frame, threshold=args["confidence"],

            # loop over the results
            for r in results:
                # extract the bounding box and box and predicted class label
                box = r.bounding_box.flatten().astype("int")
                # label = self.labels[r.label_id]
                label = Enums.Pikogramm(r.label_id)

                # Notify others
                distance = calcDistance(box, label, frame.shape[1])
                self.notify_observers(SubjectObjectDetection(label, box, datetime.now(), r.score, orig.shape, distance))

    def calcDistance(self, box, lable, imageHeight):
        #Breite/Hoehe
        dimensionsList = [[13, 12], [0, 0], [15,14], [14, 14], [14, 13], [10, 12]]

        realHigh = dimensionsList[lable][1]

        (startX, startY, endX, endY) = box
        pixelHigh = endy - startY

        return (self.FOCAL_LENGTH_MM * realHigh * imageHeight) / (pixelHigh * self.SENSOR_HEIGHT_MM)