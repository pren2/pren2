import Enums
from Hybrid.Fake.FakeSensor import Sensor
from Hybrid.Fake.FakeTransmitter import FakeTransmitter
from Hybrid.Logging import Logging


class FakeHybrid:
    def __init__(self):
        """Fakeimplemtation for TheHybrid class."""
        self.sensors = []
        self.sensors.append(Sensor(Enums.SensorPosition.topFrontLeft))
        self.sensors.append(Sensor(Enums.SensorPosition.topFrontRight))
        self.sensors.append(Sensor(Enums.SensorPosition.bottomFrontLeft))
        self.sensors.append(Sensor(Enums.SensorPosition.bottomFrontRight))
        self.transmitter = FakeTransmitter()

    def sendCommand(self, command, senderToken = None):
        #print("Send Command Fake Hybrid")
        self.transmitter.sendCommand(command, senderToken)

    def changeEmergency(self):
        self.transmitter.changeEmergency()
