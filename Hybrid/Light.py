import RPi.GPIO as GPIO
import time

class Light:
    """Class to control Light."""
    def __init__(self, pin):
        self.pin = pin

    def blink(self, blinknumber):
        for x in range(blinknumber):
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.pin,GPIO.OUT)
            GPIO.output(self.pin,GPIO.HIGH)
            time.sleep(1)
            GPIO.output(self.pin,GPIO.LOW)
            time.sleep(1)