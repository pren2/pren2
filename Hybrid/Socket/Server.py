import socket
import threading
from Enums import Command


class HybridServer:
    """Class to receive Commands form HybridClient."""
    def __init__(self, theHybrid):

        self.theHybrid = theHybrid

        # get the hostname
        host = "0.0.0.0"
        port = 5000

        self.server_socket = socket.socket()
        self.server_socket.bind((host, port))

        # configure how many client the server can listen simultaneously
        self.server_socket.listen(2)

        threading.Thread(target=self.run).start()

    def run(self):
        print("Server is lisening")
        while True:
            conn, address = self.server_socket.accept()
            data = conn.recv(1024).decode()
            if not data:
                # if data is not received break
                break
            self.sendCommand(int(data))
            conn.close()

    def sendCommand(self, value):
        if value <= 9:
            self.theHybrid.sendCommand(Command(int(value)), "server")
        if value == 10:
            self.theHybrid.sendCommand(Command.Stop, "server")
        if value == 11:
            self.theHybrid.setEmergencyOn()
            self.theHybrid.sendCommand(Command.Stop, "server")
        if value == 12:
            pass #Test Data
        if value == 13:
            self.theHybrid.sendCommand(Command.FlyHover, "server")
